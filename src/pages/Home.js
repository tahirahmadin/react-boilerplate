import Axios from 'axios';
import React, { Component } from 'react';
import Appbar from '../common/Appbar';
import Loader from '../common/Loader';
import PhotosList from '../components/PhotosList';
import InfiniteScroll from 'react-infinite-scroll-component';

let url = 'https://api.unsplash.com/photos';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      page_no: 1,
      loading: true,
    };
  }
  componentDidMount = () => {
    this.fetchData(this.state.page_no);
  };
  updatePhotosList = (data) => {
    this.setState({ photos: [...this.state.photos, ...data], loading: false });
  };
  fetchData = () => {
    Axios.get(url, {
      params: {
        client_id: 'AzcHc6F3XTqanT3hEE32tHQUx53YoiWW7zh2A1T8ePU',
        page: this.state.page_no,
      },
    }).then((res) => this.updatePhotosList(res.data));
    this.setState({ page_no: this.state.page_no + 1 });
  };
  render() {
    return (
      <div>
        <Appbar />
        {!this.state.loading ? (
          <div>
            <InfiniteScroll
              dataLength={this.state.photos.length}
              next={this.fetchData}
              hasMore={true}
              loader={<h6>Loading...</h6>}>
              <PhotosList photos={this.state.photos} />
            </InfiniteScroll>
          </div>
        ) : (
          <Loader />
        )}
      </div>
    );
  }
}
export default Home;
