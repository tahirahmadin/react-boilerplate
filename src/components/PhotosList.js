import React, { Component } from 'react';
import Masonry from 'react-masonry-css';
import PhotoModal from './PhotoModal';
import { LazyLoadImage } from 'react-lazy-load-image-component';

class PhotosList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalOpen: false,
      selectedPhotoId: 0,
    };
  }
  render() {
    return (
      <div className="mt-3 p-1">
        <Masonry breakpointCols={5} className="my-masonry-grid" columnClassName="my-masonry-grid_column">
          {this.props.photos.map((photo, index) => (
            <div
              onClick={() => this.setState({ modalOpen: true, selectedPhotoId: index })}
              style={{ cursor: 'pointer' }}>
              <LazyLoadImage
                effect="blur"
                alt="img"
                src={photo.urls.small}
                style={{ width: '19vw', borderRadius: 10 }}
              />
            </div>
          ))}
        </Masonry>
        {this.state.modalOpen ? (
          <PhotoModal
            modalOpen={this.state.modalOpen}
            handleClose={() => this.setState({ modalOpen: false })}
            photosList={this.props.photos}
            selectedPictureId={this.state.selectedPhotoId}
          />
        ) : (
          ''
        )}
      </div>
    );
  }
}
export default PhotosList;
