import React, { Component } from 'react';

export default class Loader extends Component {
  render() {
    return (
      <div>
        <img
          src="https://i.pinimg.com/originals/35/b2/5a/35b25ac379b8e771bbd3fd956f4d31bb.gif"
          alt="loader"
          height="200px"
        />
      </div>
    );
  }
}
